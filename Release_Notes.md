08/28/2020: PALISADE Encrypted Circuit Evaluator v1.0 is released

* Initial prototype release 
* Tested with PALISADE v10.3

Comments/additions are welcome by the PALISADE team

find_package( Boost )

# pece - Palisade Encrypted Circuit Emulated
add_library( pecelib 
    analyze.cpp 
    assemble.cpp 
    circuit.cpp 
    gate.cpp 
    utils.cpp 
    wire.cpp 
)

# pecetest - Palisade Encrypted Circuit Emulated - Test
add_library( pecetestlib 
    test_adder.cpp 
    test_aes.cpp 
    test_comparator.cpp 
    test_crypto.cpp 
    test_multiplier.cpp 
    test_parity.cpp 
)
target_link_libraries( pecelib pecetestlib )
target_link_libraries( pecetestlib pecelib )

add_executable( TB_adders TB_adders.cpp )
add_executable( TB_adder_2bit TB_adder_2bit.cpp )
add_executable( TB_aes TB_aes.cpp )
add_executable( TB_comparators TB_comparators.cpp )
add_executable( TB_crypto TB_crypto.cpp )
add_executable( TB_multipliers TB_multipliers.cpp )
add_executable( TB_parity TB_parity.cpp )

target_link_libraries( TB_adders pecelib pecetestlib )
target_link_libraries( TB_adder_2bit pecelib pecetestlib )
target_link_libraries( TB_aes pecelib pecetestlib )
target_link_libraries( TB_comparators pecelib pecetestlib )
target_link_libraries( TB_crypto pecelib pecetestlib )
target_link_libraries( TB_multipliers pecelib pecetestlib )
target_link_libraries( TB_parity pecelib pecetestlib )